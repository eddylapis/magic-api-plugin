# magic-api-plugin

#### 介绍
magic-api插件

#### 软件架构
软件架构说明
1.   **安全插件** 
 
-  全局限流和单个接口限流
-   IP白名单
-   第三方接口对接鉴权
-   多用户登录及用户授权

2.  **集群插件** 

-   集群监控状态
-   集群实例信息监听和同步
-   服务资源占用报警

3.   **微服务插件** 

- 支持rpc接口调用
- 本地负载均衡
- 服务降级


      

#### 使用教程

1.  在引入magic-api的pom工程中，引入插件依赖
2.  在配置文件中进行配置开关启用

```
#如开启限流插件
magic-plugin.limiter.enable=true
```


#### 使用说明

1.  安全插件

```
         <dependency>
            <groupId>org.d8s</groupId>
            <artifactId>magic-api-plugin-security-starter</artifactId>
            <version>1.1.1</version>
        </dependency>
       
```

2.  集群插件

```
        <dependency>
            <groupId>org.d8s</groupId>
            <artifactId>magic-api-plugin-cluster-starter</artifactId>
            <version>1.1.1</version>
        </dependency>
```

3.  微服务插件
```
        <dependency>
            <groupId>org.d8s</groupId>
            <artifactId>magic-api-plugin-cloud-starter</artifactId>
            <version>1.1.1</version>
        </dependency>
```

