package org.d8s.magicapi.spring.boot.starter;


import org.d8s.magicapi.aop.MagicApiClusterAspect;
import org.d8s.magicapi.listener.MagicApiInfoUpdateListener;
import org.d8s.magicapi.listener.RedisMessageListener;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import org.ssssssss.magicapi.spring.boot.starter.MagicAPIProperties;

/**
 * 接口变动通知AutoConfiguration
 * @author 冰点
 * @date 2021-5-13 10:11:35
 * @since 1.1.1
 */
@Configuration
@ConditionalOnClass({RequestMappingHandlerMapping.class})
@EnableConfigurationProperties(MagicAPIProperties.class)
@Import({MagicApiInfoUpdateListener.class, RedisMessageListener.class, MagicApiClusterAspect.class})
public class ClusterNoticeAutoConfiguration implements WebMvcConfigurer {


}
