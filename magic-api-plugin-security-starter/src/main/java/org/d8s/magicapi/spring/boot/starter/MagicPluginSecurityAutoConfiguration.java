package org.d8s.magicapi.spring.boot.starter;


import org.d8s.magicapi.plugin.interceptor.GlobalApiLimiterInterceptor;
import org.d8s.magicapi.plugin.interceptor.IpLimitRequestInterceptor;
import org.d8s.magicapi.plugin.interceptor.OpenAuthRequestInterceptor;
import org.d8s.magicapi.plugin.interceptor.SimpleAuthorizationInterceptor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import org.ssssssss.magicapi.spring.boot.starter.MagicAPIProperties;

/**
 * 安全相关的插件AutoConfiguration
 *
 * @author 冰点
 * @date 2021-5-13 10:11:35
 */
@Configuration
@ConditionalOnClass({RequestMappingHandlerMapping.class})
@EnableConfigurationProperties(MagicAPIProperties.class)
@Import({SimpleAuthorizationInterceptor.class, OpenAuthRequestInterceptor.class, GlobalApiLimiterInterceptor.class, IpLimitRequestInterceptor.class})
public class MagicPluginSecurityAutoConfiguration implements WebMvcConfigurer {


}
