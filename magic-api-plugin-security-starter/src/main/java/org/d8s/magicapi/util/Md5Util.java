package org.d8s.magicapi.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *  md5工具类
 * @author wangshuai@e6yun.com
 * @date 6/8/2021 11:13 AM
 **/
public class Md5Util {
    private static final String DEFAULT_CHARSET = "UTF-8";
    private static final int MD5_LENGTH_32 = 32;
    private static final int MD5_LENGTH_16 = 16;
    enum LetterType {
        /**
         * 大小写
         */
        LowerCase, UpperCase
    }
    /**
     * @param bytes2Md5
     * @return
     * @Description: 32位小写MD5
     */
    public static String parseStrToMd5L32(byte[] bytes2Md5) {
        String reStr = null;
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            byte[] bytes = md5.digest(bytes2Md5);
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b : bytes) {
                int bt = b & 0xff;
                if (bt < 16) {
                    stringBuffer.append(0);
                }
                stringBuffer.append(Integer.toHexString(bt));
            }
            reStr = stringBuffer.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return reStr;
    }
    /**
     * 生成 MD5
     * @param data 待处理数据
     * @param length 长度
     * @param type 大小写
     * @return MD5结果
     * @throws Exception
     */
    public static String md5(String data,int length,LetterType type) throws Exception {
        byte[] dataBytes=data.getBytes(DEFAULT_CHARSET);
        String result ="";
        if(length==MD5_LENGTH_32&&type.equals(LetterType.UpperCase)){
            result =parseStrToMd5U32(dataBytes);
        }
        if(length==MD5_LENGTH_32&&type.equals(LetterType.LowerCase)){
            result =parseStrToMd5L32(dataBytes);
        }
        if(length==MD5_LENGTH_16&&type.equals(LetterType.UpperCase)){
            result =parseStrToMd5U16(dataBytes);
        }
        if(length==MD5_LENGTH_16&&type.equals(LetterType.LowerCase)){
            result =parseStrToMd5L16(dataBytes);
        }
        return result;
    }

    /**
     * 生成 MD5
     *
     * @param data 待处理数据
     * @return MD5结果
     */
    public static String md5(String data) throws Exception {
        return md5(data,MD5_LENGTH_32, LetterType.UpperCase);
    }
    /**
     * @param bytes2Md5
     * @return
     * @Description: 32位大写MD5
     */
    public static String parseStrToMd5U32(byte[] bytes2Md5) {
        String reStr = parseStrToMd5L32(bytes2Md5);
        if (reStr != null) {
            reStr = reStr.toUpperCase();
        }
        return reStr;
    }

    /**
     * @param bytes2Md5
     * @return
     * @Description: 16位小写MD5
     */
    public static String parseStrToMd5U16(byte[] bytes2Md5) {
        String reStr = parseStrToMd5L32(bytes2Md5);
        if (reStr != null) {
            reStr = reStr.toUpperCase().substring(8, 24);
        }
        return reStr;
    }

    /**
     * @param bytes2Md5
     * @return
     * @Description: 16位大写MD5
     */
    public static String parseStrToMd5L16(byte[] bytes2Md5) {
        String reStr = parseStrToMd5L32(bytes2Md5);
        if (reStr != null) {
            reStr = reStr.substring(8, 24);
        }
        return reStr;
    }

}
